var pictureArray = ['dog', 'cat', 'mouse', 'bug', 'bat', 'frog', 'spider', 'monkey', 'fly'];
var pictureToGuess = randomPicture();

window.addEventListener("load", init);

function init() {

    showPictures();
    showPictureToGuess();

}

function showPictures() {
    for (var index = 0; index < pictureArray.length; index++) {
        var img = document.createElement('img');
        img.setAttribute('src', '/js-media/' + pictureArray[index] + '.jpg');
        img.setAttribute('class', 'img--small');
        img.setAttribute('id', pictureArray[index]);
        img.addEventListener('click', handleImgClickEvent);
        var ph = document.getElementById('img-grid');
        ph.appendChild(img);
    }
}

function handleImgClickEvent(event) {
    console.log(event.target.id);
    var chosenPicture = event.target.id;
    if (chosenPicture == pictureToGuess) {
        console.log('Lekker bezig!, probeer deze maar eens!');
        document.getElementById('message').innerHTML = 'Lekker bezig!, probeer deze maar eens!';
        pictureToGuess = randomPicture();
        showPictureToGuess();
    } else {
        console.log('Jammer, probeer het nog eens')
        document.getElementById('message').innerHTML = 'Jammer, probeer het nog eens';
    };

}

function showPictureToGuess() {
    //console.log(pictureToGuess);   
    var img = document.getElementById('random-img');
    img.setAttribute('src', '/js-media/' + pictureToGuess + '.jpg');
}


function randomPicture() {
    // console.log (Math.floor(pictureArray.length *Math.random()));
    return pictureArray[Math.floor(pictureArray.length * Math.random())];
}